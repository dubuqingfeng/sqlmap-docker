FROM python:2.7

MAINTAINER Dubu Qingfeng <1135326346@qq.com>

RUN git clone -v  https://github.com/sqlmapproject/sqlmap.git \
&& cd sqlmap


VOLUME /data

WORKDIR /sqlmap

CMD ["—h"]
ENTRYPOINT ["./sqlmap.py"]